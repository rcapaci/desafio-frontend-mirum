module.exports = function(grunt) {

	grunt.config('sass', {
		dist: {
			options: {
				style: 'expanded'
			},
			files: {
				'dist/styles/geral.css': 'source/styles/geral.scss',
				//'dist/styles/nome-do-estilo.css': 'source/styles/nome-do-estilo.scss'

				/*
					Caso seja necessário utilizar o CSS de algum plugin instalado pelo bower,
					descomentar a linha abaixo e informar o caminho do arquivo.
				*/

					'dist/styles/plugins.css': [
						'source/bower_components/select2/src/scss/core.scss'
					]
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
};
