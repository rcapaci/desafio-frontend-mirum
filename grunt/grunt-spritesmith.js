module.exports = function(grunt) {

    grunt.config('sprite', {
        all: {
            src: 'source/img/sprite-images/*.png',
            dest: 'dist/images/sprites.png',
            destCss: 'source/styles/00_base/_sprites.scss'
        }
    });

    grunt.loadNpmTasks('grunt-spritesmith');
};
