/* ------- VARIÁVEL GLOBAL DO APLICATIVO ------- */
window.app = window.app || {};

/* -- */

(function($) {

/*
--------------
VARIÁVEIS
--------------
*/

    var $document = $(document),
        $window   = $(window),
        $body     = $('body');



/*
--------------
FUNÇÕES
--------------
*/

    /*
        Scroll animado para qualquer elemento da tela
    */

    function scrollTo( $el ) {

        if( $el.length < 1 ) {
            return;
        }

        $( 'html, body' ).animate({
            scrollTop: $el.first().offset().top
        }, 300, 'linear' );
    }



/*
--------------
TRIGGERS
--------------
*/

    /*
        Documento carregado
    */
    $document.ready( function() {

        // Polyfill input placeholder
        if( !Modernizr.input.placeholder && typeof( jQuery().placeholder ) !== 'undefined' ){
            $('input, textarea').placeholder();
        }


        // Carrega LiveReload se estiver em ambiente de Desenvolvimento
        if( location.hostname.indexOf( 'localhost' ) !== -1 ) {
            var liveReload = $( '<script />' );
            liveReload
                .attr( 'src', '//localhost:35729/livereload.js' )
                .appendTo( 'body' );
        }

    });



    /*
        Página completamente carregada
    */
    $window.on( 'load', function() {});



    /*
        Resize da tela
    */
    $window.on( 'resize', function() {});



    /*
        Scroll da tela
    */
    $window.on( 'scroll', function() {});



    /*
        Scroll animado para qualquer elemento da tela
    */
    $document.on( 'click', '.js--scroll-to', function(e) {
        e.preventDefault();
        scrollTo( $( $( this ).attr( 'data-seletor' ) ) );
    });

    /*
        Galeria de fotos
    */
    $document.ready(function() {
        var _images = $('.galeria__fotos--foto');
        var _gallery = $('.gallery');
        var _galleryImage = $('.gallery__image');
        var activeImage = null;

        /* abre galeria */
        _images.on('click', function (e) {
            e.preventDefault();

            /* trava scroll do body */
            $('body').addClass('gallery__active');
            _gallery.addClass('gallery__opened');

            /* descobre a imagem que foi clicada */
            activeImage = $(this).index();

            /* seta a url da imagem mostrada */
            _galleryImage.attr('src', $(_images[activeImage]).data('img-url'));
        });


        function closeGallery() {
            $('body').removeClass('gallery__active');
            _gallery.removeClass('gallery__opened');
            activeImage = null;
        }

        /* fecha galeria */
        $('.gallery__close').on('click', function (e) {
            e.preventDefault();
            closeGallery();
        });


        $(document).keyup(function(e) {
            if (e.keyCode === 27) { // escape key maps to keycode `27`
                if (_gallery.hasClass('gallery__opened')) {
                    closeGallery();
                }
            }
        });



        $('.gallery.gallery__opened').on('click', function (e) {
            e.preventDefault();
            $('body').removeClass('gallery__active');
            _gallery.removeClass('gallery__opened');
            activeImage = null;
        });

        /* troca de imagem para a esquerda */
        $('.gallery__left').on('click', function (e) {
            e.preventDefault();

            if (activeImage === 0) {
                activeImage = _images.length - 1;
            } else {
                activeImage--;
            }
            _galleryImage.attr('src', $(_images[activeImage]).data('img-url'));
        });

        /* troca de imagem para a direita */
        $('.gallery__right').on('click', function (e) {
            e.preventDefault();

            activeImage = (activeImage + 1) % _images.length;
            _galleryImage.attr('src', $(_images[activeImage]).data('img-url'));
        });


        var _formHeader = $('.form-header');
        var _headerFormOpenLink = $('#header__form--open-link');

        /* abre e fecha formulario do header */
        _headerFormOpenLink.on('click touchstart', function (e) {
            e.preventDefault();
            _formHeader.toggleClass('form-header__opened');
            $('.header__form').toggleClass('header__form--opened');

            if (_formHeader.hasClass('form-header__opened')) {
                $('.form-header__label--input')[0].focus();
                _headerFormOpenLink.prop('title', 'Fechar formulário de contato');
            } else {
                _headerFormOpenLink.focus();
                _headerFormOpenLink.prop('title', 'Preencher formulário de contato');
            }
        });

    });

})(jQuery);


/**
 * Google Maps
 */
function initMap() {
    var position = {lat: -25.4404044, lng: -49.302451};

    var map = new google.maps.Map(document.getElementById('google-map'), {
        backgroundColor: '#ccc',
        zoom: 18,
        center: position,
        disableDefaultUI: true
    });

    var marker = new google.maps.Marker({
        position: position,
        map: map
    });
}
