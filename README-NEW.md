Framework FED

O objetivo do Framework é padronizar o trabalho realizado pela equipe de Front-End Development para os projetos desenvolvidos pela Agência.

Este framework foi criado já utilizando Grunt, que é um automatizador de tarefas visando facilitar o trabalho dos desenvolvedores.

Para utilizá-lo pela primeira vez, precisamos fazer a instalação de algumas ferramentas necessárias para correta utilização, conforme:

- NodeJS - [nodejs]: http://nodejs.org/
- Grunt-Cli - [grunt-cli]: http://gruntjs.com/getting-started
- SASS - [sass]: http://sass-lang.com/install/
- BrowserSYNC - [browsersync]: http://browsersync.io/
- Bower - [bower]: http://bower.io/

A instalação destes componentes precisa ser realizada apenas uma vez.



